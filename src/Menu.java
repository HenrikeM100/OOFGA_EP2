import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

public class Menu{
	private JFrame framePrincipal;
	private JLabel headerLabel;
	private JPanel controlPanel;
	private JLabel statusLabel;
	public static int [] rankingScore = new int[0];
	public static String [] rankingName = new String[0];
	private DefaultTableModel model;
	
	public Menu(){
		preparaGUI();
		montaTela();
	}

	public static void main(String[] args){
		File score = new File("score.txt");
		try{
			if(!score.exists()) {
				score.createNewFile();
				FileWriter arq = new FileWriter("score.txt");
				PrintWriter gravarArq = new PrintWriter(arq);
				gravarArq.println("nome score");
				gravarArq.println("0");
				gravarArq.close();
			}
			else{
				FileReader arq = new FileReader(score);
				BufferedReader lerArq = new BufferedReader(arq);
				String line = lerArq.readLine();
				Scanner lerDados = new Scanner(lerArq);
				int quantidade = lerDados.nextInt();
				if(quantidade != 0) {
					rankingScore = new int[quantidade];
					rankingName = new String[quantidade];
					for(int i = 0; i < quantidade; ++i) {
						rankingName[i] = lerDados.next();
						rankingScore[i] = lerDados.nextInt();
					}
				}
				lerDados.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		Menu telaPrincipal = new Menu();
		telaPrincipal.framePrincipal.setVisible(true);
	}
	
	public void preparaGUI(){
		framePrincipal = new JFrame("Batalha Naval 2.0");
	    framePrincipal.setSize(500,500);
	    framePrincipal.setLayout(new GridLayout(3,1));
	    framePrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    framePrincipal.setLocationRelativeTo(null);
	      
	    headerLabel = new JLabel("",JLabel.CENTER);
	    statusLabel = new JLabel("",JLabel.CENTER);
	      
	    controlPanel = new JPanel();
	    controlPanel.setLayout(new GridLayout(4,1));
	      
	    framePrincipal.add(headerLabel);
	    framePrincipal.add(controlPanel);
	    framePrincipal.add(statusLabel);
	}
	
	public void montaTela(){
	    headerLabel.setText("Bem-Vindo ao Batalha Naval 2.0!");
	      
	    JButton playButton = new JButton("JOGAR");
	    JButton scoreButton = new JButton("RANKING");
	    JButton tutorialButton = new JButton("TUTORIAL");
	    JButton exitButton = new JButton("ENCERRAR");
	      
	    playButton.setActionCommand("play");
	    scoreButton.setActionCommand("score");
	    tutorialButton.setActionCommand("tutorial");
	    exitButton.setActionCommand("encerrar");
	      
	    playButton.addActionListener(new Listener());
	    scoreButton.addActionListener(new Listener());
	    tutorialButton.addActionListener(new Listener());
	    exitButton.addActionListener(new Listener());
	     
	    controlPanel.add(playButton);
	    controlPanel.add(scoreButton);
	    controlPanel.add(tutorialButton);
	    controlPanel.add(exitButton);
	}
	
	private class Listener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			String command = e.getActionCommand();
			if(command.equals("play")){
				lerArquivo();
			}
			else if (command.equals("score")){
				verScore();
			}
			else if (command.equals("tutorial")){
				JOptionPane.showMessageDialog(null, tutorial);
			}
			else {
				System.exit(0);
			} 
		} 
	}
	
	public void lerArquivo() {
		try {
			JFileChooser chooser = new JFileChooser();
			chooser.setFileFilter(new FileNameExtensionFilter("Apenas .txt", "txt"));
			int retorno = chooser.showOpenDialog(null);
			if (retorno == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				FileReader arq = new FileReader(file);
				BufferedReader lerArq = new BufferedReader(arq);
				String line = lerArq.readLine();
				if(line.equals("# width height")){
					Scanner lerDados = new Scanner(lerArq);
					int colunas = lerDados.nextInt();
					int linhas = lerDados.nextInt();
					for(int i = 0; i < 3; ++i) {
						line = lerDados.nextLine();
					}
					JogoInterface jogoTela = new JogoInterface(linhas, colunas);
					for(int i = 0; i < linhas; ++i) {
						line = lerDados.nextLine();
						for(int j = 0; j < colunas; ++j) {
							int num = line.charAt(j)-48;
							jogoTela.jogo.setPosMatriz(num, i, j);
						}
					}
					for(int i = 0; i < 2; ++i) {
						line = lerDados.nextLine();
					}
					int total = 0;
					for(int i = 0; i < 5; ++i) {
						int num = lerDados.nextInt();
						num = lerDados.nextInt();
						jogoTela.jogo.setEmbQuant(i, num);
						total += (i+1)*num;
					}
					jogoTela.jogo.setTotalEmbarc(total);
					jogoTela.terminaAjustes();
					lerDados.close();
					lerArq.close();
				}
				else{
					lerArq.close();
					throw new IllegalArgumentException();
				}
			}
		}
		catch (IOException erroAbertura) {
			statusLabel.setText("Erro na abertura do arquivo");
	    }
		catch (IllegalArgumentException erro) {
			statusLabel.setText("Arquivo inválido, selecione outro");
		}
	}
	
	private void verScore() {
		int quantidade = rankingScore.length;
		
		model = new DefaultTableModel();
		
		JFrame rankFrame = new JFrame("Ranking");
		rankFrame.setLayout(new GridLayout(1, 1));
		rankFrame.setSize(500,500);
		rankFrame.setLocationRelativeTo(null);
		
		JPanel rankPanel = new JPanel();
		rankPanel.setLayout(new GridLayout(1, 1));
		
		JTable tabela = new JTable(model);
		model.addColumn("Posição");
		model.addColumn("Nome");
		model.addColumn("Score");
		
		if(quantidade == 0) {
			JLabel semRank = new JLabel("NÃO HÁ NENHUM JOGADOR NO RANKING",JLabel.CENTER);
			rankFrame.add(semRank);
		}
		else {				
			for(int i = 0; i < rankingScore.length; ++i) {
				model.addRow(new Object[]{i+1, rankingName[i], rankingScore[i]});
			}
			
			JScrollPane scroll = new JScrollPane(tabela);
			
			rankPanel.add(scroll);
			rankFrame.add(rankPanel);
		}
		rankFrame.setVisible(true);
	}
	
	public static void atualizarScore(int score, String nome) {
		int i = rankingScore.length;
		int [] novoRankingScore = new int[i+1];
		String [] novoRankingNome = new String[i+1];
		
		if(i == 0) {
			novoRankingScore[i] = score;
			novoRankingNome[i] = nome;
			rankingScore = novoRankingScore;
			rankingName = novoRankingNome;
		}
		else if(score < rankingScore[i-1]) {
			novoRankingScore[i] = score;
			novoRankingNome[i] = nome;
			i--;
			while(i >= 0) {
				novoRankingScore[i] = rankingScore[i];
				novoRankingNome[i] = rankingName[i];
				i--;
			}
			rankingScore = novoRankingScore;
			rankingName = novoRankingNome;
		}
		else {
			int j = i-1;
			while(j >= 1) {
				novoRankingScore[i] = rankingScore[j];
				novoRankingNome[i] = rankingName[j];
				if(score >= rankingScore[j] && score < rankingScore[j-1]) {
					i--;
					novoRankingScore[i] = score;
					novoRankingNome[i] = nome;
				}
				i--;
				j--;
			}
			novoRankingScore[i] = rankingScore[j];
			novoRankingNome[i] = rankingName[j];
			if(score >= rankingScore[j]) {
				novoRankingScore[0] = score;
				novoRankingNome[0] = nome;
			}
		}
		
		rankingScore = novoRankingScore;
		rankingName = novoRankingNome;
		
		atualizaTabelaRanking();
	}
	
	public static void atualizaTabelaRanking() {
		File score = new File("score.txt");
		score.delete();
		try {
			score.createNewFile();
			FileWriter arq = new FileWriter("score.txt");
			PrintWriter gravarArq = new PrintWriter(arq);
			gravarArq.println("nome score");
			gravarArq.println(rankingScore.length);
			for(int i = 0; i < rankingScore.length; ++i) {
				gravarArq.println(rankingName[i] + " " + rankingScore[i]);
			}
			gravarArq.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String tutorial = "TUTORIAL:\n\n"
			
			+         "Regras do jogo: Você deve acertar todos os alvos antes de ficar sem energia.\n"
			+         "                              Selecione os comandos abaixo do mapa e atire onde desejar.\n"
			+         "                              Abaixo dos comandos há a quantidade de embarcações de cada tipo\n"
			+         "                              e o total de alvos.\n\n"
			+         "Requisitos de vitória: Para vencer, você precisa derrotar todos os alvos antes de ficar sem\n"
			+         "                                        energia.\n"
			+         "                                        Se você não tiver energia suficiente para realizar uma ação e\n"
			+         "                                        ainda houver alvos, você perde.\n\n"
			+         "Legenda de cores: \n"
			+         "Quadrado azul: Local em que ainda não sofreu nenhuma ação.\n"
			+         "Quadrado verde: Se houver um alvo no local do tiro, o quadrado azul passará a ser verde.\n"
			+         "Quadrado vermelho: Se não houver um alvo no local do tiro, o quadrado azul passará a ser vermelho.\n"
			+         "\"X\" Vermelho: Se no local em que foi usado a função revelar não tiver um alvo, o local terá um \"X\".\n"
			+         "\"O\" Verde: Se no local em que foi usado a função revelar tiver um alvo, o local terá um \"O\".\n"
			
			;
}