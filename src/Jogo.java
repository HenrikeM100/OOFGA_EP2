import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JOptionPane;

public class Jogo extends Canvas{
	private int playerScore = 0;
	private int playerEnergia;
	private int maxEnergy;
	private int linhas;
	private int colunas;
	private int [][] matriz;
	private int rect_vert;
	private int rect_hor;
	public String comando = "local";
	private int [] embarc_quant = new int[5];
	private int totalEmbarc;
	private int [] custoEnergia = new int[5];
	
	public Jogo(int linhas, int colunas) {
		setLinhas(linhas);
		setColunas(colunas);
		
		rect_vert = 700/linhas;
		rect_hor = 700/colunas;	
		
		int [][] matriz = new int[linhas][colunas];
		setMatriz(matriz);
		
		custoEnergia[0] = 10;
		custoEnergia[1] = 25;
		custoEnergia[2] = 35;
		custoEnergia[3] = 7*(linhas);
		custoEnergia[4] = 7*(colunas);
	}
	
	@Override
	public void paint(Graphics g) {
		rect_vert = getRect_vert();
		rect_hor = getRect_hor();
        
		for(int i = 0; i < colunas; i++)
			for(int j = 0; j < linhas; ++j) {
				g.setColor(Color.CYAN);
		        g.fillRect(i*rect_hor, j*rect_vert, rect_hor, rect_vert);
		        g.setColor(Color.BLACK);
		        g.drawRect(i*rect_hor, j*rect_vert, rect_hor, rect_vert);
			}
	}

	public int getPlayerScore() {
		return playerScore;
	}

	public void setPlayerScore(int playerScore) {
		this.playerScore = playerScore;
	}

	public int getLinhas() {
		return linhas;
	}

	public void setLinhas(int linhas) {
		this.linhas = linhas;
	}

	public int getColunas() {
		return colunas;
	}

	public void setColunas(int colunas) {
		this.colunas = colunas;
	}

	public void setPlayerEnergia(int playerEnergia) {
		this.playerEnergia = playerEnergia;
	}

	public int getPlayerEnergia() {
		return playerEnergia;
	}

	public int getMatriz(int x, int y) {
		return matriz[x][y];
	}

	public void setMatriz(int[][] matriz) {
		this.matriz = matriz;
	}
	
	public int getRect_vert() {
		return rect_vert;
	}

	public void setRect_vert(int rect_vert) {
		this.rect_vert = rect_vert;
	}

	public int getRect_hor() {
		return rect_hor;
	}

	public void setRect_hor(int rect_hor) {
		this.rect_hor = rect_hor;
	}
	
	public void setPosMatriz(int valor, int x, int y) {
		this.matriz[x][y] = valor;
	}
	
	public void setEmbQuant(int x, int quant) {
		this.embarc_quant[x] = quant;
	}
	
	public int getEmbQuant(int x){
		return embarc_quant[x];
	}
	
	public int getCustoEnergia(int x) {
		return custoEnergia[x];
	}
	
	public int getMaxEnergy() {
		return maxEnergy;
	}

	public void setMaxEnergy(int maxEnergy) {
		this.maxEnergy = maxEnergy;
	}

	public void verifComando(int coluna_pos, int linha_pos) {
		if(comando.equals("local")){
			if(playerEnergia >= custoEnergia[0]) {
				atirar(coluna_pos, linha_pos);
				playerEnergia -= custoEnergia[0];
			}
			else
				JOptionPane.showMessageDialog(null, noEnergy);
		}
		else if(comando.equals("revelar")){
			if(playerEnergia >= custoEnergia[1]) {
				revelar(coluna_pos, linha_pos);
				
				if(linha_pos+1 < linhas)
					revelar(coluna_pos, linha_pos+1);
				
				if(coluna_pos+1 < colunas)
					revelar(coluna_pos+1, linha_pos);
				
				if(coluna_pos+1 < colunas && linha_pos+1 < linhas)
					revelar(coluna_pos+1, linha_pos+1);
				
				playerEnergia -= custoEnergia[1];
			}
			else
				JOptionPane.showMessageDialog(null, noEnergy);
		}
		else if(comando.equals("area")){
			if(playerEnergia >= custoEnergia[2]) {
				atirar(coluna_pos, linha_pos);
				
				if(linha_pos+1 < linhas)
					atirar(coluna_pos, linha_pos+1);
				
				if(coluna_pos+1 < colunas)
					atirar(coluna_pos+1, linha_pos);
				
				if(coluna_pos+1 < colunas && linha_pos+1 < linhas)
					atirar(coluna_pos+1, linha_pos+1);
				
				playerEnergia -= custoEnergia[2];
			}
			else
				JOptionPane.showMessageDialog(null, noEnergy);
		}
		else if(comando.equals("linha")){
			if(playerEnergia >= custoEnergia[3]) {
				for(int i = 0; i < colunas; ++i) {
					atirar(i, linha_pos);
				}
				
				playerEnergia -= custoEnergia[3];
			}
			else
				JOptionPane.showMessageDialog(null, noEnergy);
		}
		else if(comando.equals("coluna")){
			if(playerEnergia >= custoEnergia[4]) {
				for(int i = 0; i < linhas; ++i) {
					atirar(coluna_pos, i);
				}
				
				playerEnergia -= custoEnergia[4];
			}
			else
				JOptionPane.showMessageDialog(null, noEnergy);
		}
	}

	public int getTotalEmbarc() {
		return totalEmbarc;
	}

	public void setTotalEmbarc(int totalEmbarc) {
		this.totalEmbarc = totalEmbarc;
	}

	public void atirar(int x, int y) {
		Graphics g = getGraphics();
		if(matriz[y][x] == 0) {
			g.setColor(Color.RED);
	        g.fillRect(x*rect_hor, y*rect_vert, rect_hor, rect_vert);
	        playerScore -= 5;
		}
		else if(matriz[y][x] != 0 && matriz[y][x] != -1){
			Color myColor1 = new Color(0, 200, 0);
			g.setColor(myColor1);
	        g.fillRect(x*rect_hor, y*rect_vert, rect_hor, rect_vert);
	        totalEmbarc -= 1;
	        playerScore += 15;
		}
		g.setColor(Color.BLACK);
        g.drawRect(x*rect_hor, y*rect_vert, rect_hor, rect_vert);
        
        matriz[y][x] = -1;
	}
	
	public void revelar(int x, int y) {
		int tamanho = min(rect_hor, rect_vert);
		Graphics g = getGraphics();
		if(matriz[y][x] == 0) {
			g.setColor(Color.RED);
			g.setFont(new Font("Arial", Font.BOLD, tamanho));
			g.drawString("X", x*rect_hor, y*rect_vert+rect_vert);
		}
		else if(matriz[y][x] != 0 && matriz[y][x] != -1){
			Color myColor1 = new Color(0, 200, 0);
			g.setColor(myColor1);
			g.setFont(new Font("Arial", Font.BOLD, tamanho));
			g.drawString("O", x*rect_hor, y*rect_vert+rect_vert);
		}
		g.setColor(Color.BLACK);
        g.drawRect(x*rect_hor, y*rect_vert, rect_hor, rect_vert);
	}
	
	private int min(int x, int y) {
		if(x < y)
		 return x;
		
		return y;
	}
	
	private String noEnergy = "Você não tem energia o suficiente para utilizar esse comando.";
}
