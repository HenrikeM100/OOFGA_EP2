import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class JogoInterface extends JFrame{
	public Jogo jogo;
	private JPanel topPanel;
	private JPanel middlePanel;
	private JPanel bottomPanel;
	private JLabel statusLocal;
	private JLabel statusShow;
	private JLabel statusArea;
	private JLabel statusLine;
	private JLabel statusColumn;
	private JLabel statusTOTAL;
	private JProgressBar energia;
	
	public JogoInterface(int linhas, int colunas) {
		jogo = new Jogo(linhas, colunas);
		preparaGUI(jogo);
		montaMiddle();
	    
	    add(topPanel, BorderLayout.PAGE_START);
		add(middlePanel, BorderLayout.CENTER);
		add(bottomPanel, BorderLayout.PAGE_END);
		
		setVisible(true);
		
		jogo.paint(jogo.getGraphics());
		
		jogo.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
		        int coluna=e.getX();
		        int linha=e.getY();
		        
		        int coluna_pos = coluna/jogo.getRect_hor();
		        int linha_pos = linha/jogo.getRect_vert();		     
		        
		        if(linha_pos < jogo.getLinhas() && coluna_pos < jogo.getColunas()) {
		        	if(jogo.getMatriz(linha_pos, coluna_pos) != -1) {
		        		jogo.verifComando(coluna_pos, linha_pos);
			        	ajustaEnergia();
			        	setTotalText();
		        	}
		        	else
		        		JOptionPane.showMessageDialog(null, tiroRepetido);
		        }
		        
		        if(jogo.getPlayerEnergia() < 10 && jogo.getTotalEmbarc() != 0) {
		        	JOptionPane.showMessageDialog(null, mensgDerrota);
		        	dispose();
		        }
		        
		        if(jogo.getTotalEmbarc() == 0) {
		        	TelaVitoria tela = new TelaVitoria(jogo);
		        	dispose();
		        }
			}

			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}


		});
	}
	
	public void preparaGUI(Jogo jogo) {
		setTitle("Batalha Naval 2.0");
		setSize(1000,1000);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());
		
		topPanel = new JPanel();
		topPanel.setPreferredSize(new Dimension(720, 720));
		topPanel.setLayout(new BorderLayout());
		
		middlePanel = new JPanel();
		middlePanel.setLayout(new GridLayout(2, 5));
		
		bottomPanel = new JPanel();
	    bottomPanel.setPreferredSize(new Dimension(100, 100));
	    bottomPanel.setLayout(new GridLayout(3, 2));
	    
	    topPanel.add(jogo, BorderLayout.CENTER);
	}
	
	public void montaMiddle() {
		JButton localButton = new JButton("ATACAR LOCAL  [" + jogo.getCustoEnergia(0) + "]");
		JButton showButton = new JButton("REVELAR AREA  [" + jogo.getCustoEnergia(1) + "]");
		JButton areaButton = new JButton("ATACAR AREA  [" + jogo.getCustoEnergia(2) + "]");
	    JButton lineButton = new JButton("ATACAR LINHA  [" + jogo.getCustoEnergia(3) + "]");
	    JButton columnButton = new JButton("ATACAR COLUNA  [" + jogo.getCustoEnergia(4) + "]");
	    
	    localButton.setActionCommand("local");
	    showButton.setActionCommand("revelar");
	    areaButton.setActionCommand("area");
	    lineButton.setActionCommand("linha");
	    columnButton.setActionCommand("coluna");
	    
	    localButton.setSize(new Dimension(100, 100));
	    showButton.setSize(new Dimension(100, 100));
	    areaButton.setSize(new Dimension(100, 100));
	    lineButton.setSize(new Dimension(100, 100));
	    columnButton.setSize(new Dimension(100, 100));
	    
	    localButton.addActionListener(new Listener());
	    showButton.addActionListener(new Listener());
	    areaButton.addActionListener(new Listener());
	    lineButton.addActionListener(new Listener());
	    columnButton.addActionListener(new Listener());
	    
	    middlePanel.add(localButton);
	    middlePanel.add(showButton);
	    middlePanel.add(areaButton);
	    middlePanel.add(lineButton);
	    middlePanel.add(columnButton);
	    
	    statusLocal = new JLabel("SELECIONADO",JLabel.CENTER);
	    statusShow = new JLabel("",JLabel.CENTER);
	    statusArea = new JLabel("",JLabel.CENTER);
	    statusLine = new JLabel("",JLabel.CENTER);
	    statusColumn = new JLabel("",JLabel.CENTER);
	    
	    middlePanel.add(statusLocal);
	    middlePanel.add(statusShow);
	    middlePanel.add(statusArea);
	    middlePanel.add(statusLine);
	    middlePanel.add(statusColumn);
	}
	
	public void montaBottom() {		
		for(int i = 0; i < 5; ++i) {
			JLabel statusEmbarc = new JLabel("EMBARCAÇÕES DE TAMANHO [" + (i+1) + "]:   " + jogo.getEmbQuant(i),JLabel.CENTER);
			bottomPanel.add(statusEmbarc);
		}
		
		statusTOTAL = new JLabel("ALVOS RESTANTES:   " + jogo.getTotalEmbarc(),JLabel.CENTER);
		bottomPanel.add(statusTOTAL);
	}
	
	public void setTotalText() {
		statusTOTAL.setText("ALVOS RESTANTES:   " + jogo.getTotalEmbarc());
	}
	
	public void terminaAjustes() {
		montaBottom();
		jogo.setPlayerEnergia(jogo.getTotalEmbarc()*20);
		jogo.setMaxEnergy(jogo.getPlayerEnergia());
		
		energia = new JProgressBar(JProgressBar.VERTICAL, 0, jogo.getMaxEnergy());
	    energia.setStringPainted(true);
	    energia.setString("" + jogo.getMaxEnergy());
	    energia.setValue(jogo.getMaxEnergy());
	    energia.setPreferredSize(new Dimension(150,100));
	    
	    topPanel.add(energia, BorderLayout.LINE_END);
	}
	
	public void ajustaEnergia() {
		energia.setString("" + jogo.getPlayerEnergia());
		energia.setValue(jogo.getPlayerEnergia());
	}
	
	private class Listener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			String command = e.getActionCommand();
			if(command.equals("local")){
				jogo.comando = "local";
				statusLocal.setText("SELECIONADO");
				statusShow.setText("");
				statusArea.setText("");
				statusLine.setText("");
				statusColumn.setText("");
			}
			else if (command.equals("revelar")){
				jogo.comando = "revelar";
				statusLocal.setText("");
				statusShow.setText("SELECIONADO");
				statusArea.setText("");
				statusLine.setText("");
				statusColumn.setText("");
			}
			else if (command.equals("area")){
				jogo.comando = "area";
				statusLocal.setText("");
				statusShow.setText("");
				statusArea.setText("SELECIONADO");
				statusLine.setText("");
				statusColumn.setText("");
			}
			else if (command.equals("linha")){
				jogo.comando = "linha";
				statusLocal.setText("");
				statusShow.setText("");
				statusArea.setText("");
				statusLine.setText("SELECIONADO");
				statusColumn.setText("");
			}
			else if (command.equals("coluna")){
				jogo.comando = "coluna";
				statusLocal.setText("");
				statusShow.setText("");
				statusArea.setText("");
				statusLine.setText("");
				statusColumn.setText("SELECIONADO");
			}
		} 
	}
	
	private String mensgDerrota = "DERROTA";
	private String tiroRepetido = "Você já atirou neste local, escolha outro";
}
