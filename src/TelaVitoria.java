import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.awt.*;

public class TelaVitoria {
	private JFrame framePrincipal;
	private JPanel topPanel;
	private JPanel middlePanel;
	private JPanel bottomPanel;
	private JLabel mensagemVitoria;
	private JLabel pontuacao;
	private JLabel inserirNome;
	private JTextField campoNome;
	private JLabel nomeVazio;
	private Jogo jogo;
	
	public TelaVitoria(Jogo jogo) {
		this.jogo = jogo;
		preparaGUI();
		montaTela(jogo);
	}
	
	private void preparaGUI() {
		framePrincipal = new JFrame("VITÓRIA");
	    framePrincipal.setSize(500,500);
	    framePrincipal.setLayout(new GridLayout(3,1));
	    framePrincipal.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    framePrincipal.setLocationRelativeTo(null);
	    
	    topPanel = new JPanel();
	    topPanel.setLayout(new GridLayout(2,1));
	    
	    middlePanel = new JPanel();
	    middlePanel.setLayout(new GridLayout(3,1));
	    
	    bottomPanel = new JPanel();
	    bottomPanel.setLayout(new FlowLayout());
	    
	    framePrincipal.add(topPanel);
	    framePrincipal.add(middlePanel);
	    framePrincipal.add(bottomPanel);
	    
	    framePrincipal.setVisible(true);
	}

	private void montaTela(Jogo jogo) {
		mensagemVitoria = new JLabel("",JLabel.CENTER);
		mensagemVitoria.setText("Parabéns pela vitória!\n");
		
		pontuacao = new JLabel("",JLabel.CENTER);
		pontuacao.setText("Você fez " + jogo.getPlayerScore() + " pontos.");
		
		topPanel.add(mensagemVitoria);
		topPanel.add(pontuacao);
		
		inserirNome = new JLabel("",JLabel.CENTER);
		inserirNome.setText("Insira seu nome");
		
		campoNome = new JTextField();
		
		nomeVazio = new JLabel("",JLabel.CENTER);
		
		middlePanel.add(inserirNome);
		middlePanel.add(campoNome);
		middlePanel.add(nomeVazio);
		
		JButton sendButton = new JButton("ENVIAR");
	    JButton cancelButton = new JButton("CANCELAR");
	      
	    sendButton.setActionCommand("enviar");
	    cancelButton.setActionCommand("cancelar");
	      
	    sendButton.addActionListener(new Listener());
	    cancelButton.addActionListener(new Listener());
	    
	    bottomPanel.add(sendButton);
	    bottomPanel.add(cancelButton);
	}
	
	private class Listener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			String command = e.getActionCommand();
			if(command.equals("enviar")){
				String nome = campoNome.getText();
				if(nome.equals("")) {
					nomeVazio.setText("Este campo não pode estar vazio");
				}
				else {
					Menu.atualizarScore(jogo.getPlayerScore(), nome);
					framePrincipal.dispose();
				}
			}
			else {
				framePrincipal.dispose();
			} 
		} 
	}
}
