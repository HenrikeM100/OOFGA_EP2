import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class JogoTest {
	public static int linhas;
	public static int colunas;
	public static Jogo jogo;
	
	@BeforeClass
	public static void simularArquivo() {
		linhas = 7;
		colunas = 10;
		jogo = new Jogo(linhas, colunas);
	}

	@Test
	public void testaConstrutor() {
		assertEquals(linhas,jogo.getLinhas());
		assertEquals(colunas,jogo.getColunas());
		
		int rect_vert = 100;
		int rect_hor = 70;
		
		assertEquals(rect_vert,jogo.getRect_vert());
		assertEquals(rect_hor,jogo.getRect_hor());
		
		int [] custoEnergia = new int[5];
		
		custoEnergia[0] = 10;
		custoEnergia[1] = 25;
		custoEnergia[2] = 35;
		custoEnergia[3] = 7*(linhas);
		custoEnergia[4] = 7*(colunas);
		
		assertEquals(custoEnergia[0],jogo.getCustoEnergia(0));
		assertEquals(custoEnergia[1],jogo.getCustoEnergia(1));
		assertEquals(custoEnergia[2],jogo.getCustoEnergia(2));
		assertEquals(custoEnergia[3],jogo.getCustoEnergia(3));
		assertEquals(custoEnergia[4],jogo.getCustoEnergia(4));
	}
	
	@Test
	public void testaGetters() {
		jogo.setPlayerEnergia(100);
		assertEquals(100,jogo.getPlayerEnergia());
		jogo.setLinhas(7);
		assertEquals(7,jogo.getLinhas());
		jogo.setColunas(10);
		assertEquals(10,jogo.getColunas());
		jogo.setPlayerScore(100);
		assertEquals(100,jogo.getPlayerScore());
		jogo.setRect_vert(100);
		assertEquals(100,jogo.getRect_vert());
		jogo.setRect_hor(70);
		assertEquals(70,jogo.getRect_hor());
		jogo.setEmbQuant(0, 15);
		assertEquals(15,jogo.getEmbQuant(0));
		jogo.setMaxEnergy(300);
		assertEquals(300,jogo.getMaxEnergy());
		jogo.setTotalEmbarc(20);
		assertEquals(20,jogo.getTotalEmbarc());
	}
}
